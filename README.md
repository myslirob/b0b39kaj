Popis: Bude se jednat o SPA aplikaci využívající react a redux. 
Aplikace bude po přihlášení zobrazovat data ze serveru, který bude posílat informaci o stavu účtu a otevřených pozicích. 
Detail pozice bude obsahovat graf s historií ceny. 
Uživatel se v aplikaci může také registrovat, tj. vyplní údaje k přihlášení na účet u brokera XTB, které se ihned přes websocket kontrolují, 
a email s heslem pro přihlášení do aplikace.


Použití: 
Je nutné mít nainstalovaný NPM.
Spuštění je příkazem "npm start"
Zkompilování je příkazem "npm run-script build"

Z důvodu absence serveru formulář Login po pokusu o přihlášení přepne uživatele na "#/forex", po přihlášení se ozve zvuk.
Registrace ověřuje broker number a broker password přes websocket na adrese "wss://ws.xapi.pro/demo". Při správně vyplněných údajích vypíše "Server error"
z důvodu absence serveru.
Na stránce "#/forex" je zapnutý interval, který simuluje data ze serveru. 
Jsou zde informace o pozicích, které lze rozkliknout do detailu.
Po kliknutí na show detail se zobrazí graf historie pozice. Graf zmizí při kliknutí na tmavé pozadí.
Rozliknout lze také stav účtu v horním menu.

Celý web je responzivní (při 800px šířky změna rozložení).