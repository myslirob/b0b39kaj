import React, { Component } from 'react';
import './css/style.css';
import {Router} from "react-router";
import Routing from "./util/Routing";
import {Provider} from "react-redux";
import appStore from "./store/Store";
import MainComponent from "./component/MainComponent";

class App extends Component {
  construct(){

  }
  render() {
    return <Provider store={appStore}>
      <Router history={Routing._history}>
        <MainComponent/>
      </Router>
    </Provider>;
  }
}

export default App;
