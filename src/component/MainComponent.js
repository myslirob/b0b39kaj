import ForexComponent from "./ForexComponent";
import LoginComponent from "./LoginComponent";
import RegisterComponent from "./RegisterComponent";
import {Route, Switch} from "react-router";
import React from 'react';
import {withRouter} from "react-router";
import {connect} from "react-redux";
import Routes from "../util/Routes";
import styled from "styled-components";
import backgroundImage from "../images/background.jpg";

class MainComponent extends React.Component{
    constructor(prop){
        super(prop);
        console.log(this.props.location.pathname);
    }

    render(){
        const Container = styled.div`
            width: 100%;
            height: 100vh;
            ${() => (this.props.location.pathname !== Routes.forex.path && `
                background: black url(${backgroundImage}) center center fixed no-repeat;
          `)}
        `;
        return(
            <Container>
                <Switch>
                    <Route path={Routes.forex.path} component={ForexComponent}/>
                    <Route path={Routes.register.path} component={RegisterComponent}/>
                    <Route exact={true} component={LoginComponent}/>
                </Switch>
            </Container>
        );
    }
}
export default withRouter(connect()(MainComponent));
                