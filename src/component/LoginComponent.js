import React from 'react';
import {withRouter} from "react-router";
import {connect} from "react-redux";
import Routing from "../util/Routing";
import Routes from "../util/Routes";
import {loginAction, authAction} from "../store/action/UserAction";
const axios = require('axios');
axios.defaults.withCredentials = true;

class LoginComponent extends React.Component{
    constructor(prop){
        super(prop);
        this.state = {error: ""};
    }
    login(){
        this.animateOut();
        this.toForex();
        return; // for debug automatic login
        this.props.login({mail: this.mail.value, pass: this.pass.value}).then((res) => {
            if(res.success) {
                this.animateOut();
                this.toForex();
            } else {
                this.pass.value = "";
                this.setState({error: res.errorMessage});
            }
        }).catch((res) => this.setState({error: "Can't connect to server"}));



    }
    animateOut(){
        const center = document.getElementsByClassName("center").item(0);
        center.classList.add("rotate-out");
    }
    toForex(){
        this.animateOut();
        setTimeout(() => {
            document.getElementById("loading").className="active";
        }, 300);
        setTimeout(() => {
            Routing.transitionTo(Routes.forex);
        }, 900);
    }
    toRegister(){
        const center = document.getElementsByClassName("center").item(0);
        center.classList.add("rotate-out");
        setTimeout(() => { Routing.transitionTo(Routes.register); }, 900);
    }
    animateIn(){
        const center = document.getElementsByClassName("center").item(0);
        center.classList.add("rotate-in");
        setTimeout(()=> {
            center.classList.remove("rotate-in");

        }, 900);
    }
    componentDidMount() {
        this.animateIn();
        this.props.auth().then((res) => {
            console.log(res);
            if(res.data.status === "AUTHORIZED" && this.props.user.number !== void 0) { // Uzivatel uz je prihlaseny proto prepnuti na #/forex
                console.log("AUTHORIZED");
                this.toForex();
            }
        }).catch((res) => {
            if(res.response === void 0){
                //this.setState({error: "Server error"});
                console.log("Can't connect to server");
            } else {
                console.log(res.response.data.message);
            }
        });
    }

    render(){
        return(
                <div className="center">
                    <h1>Login</h1>
                    <div className="form" >
                        <form method="post" action="" >
                            <label htmlFor="name" className="label-name"><input onKeyPress={(e) => { if(e.key === 'Enter') this.login();}} ref={inp => this.mail = inp} defaultValue={this.props.user.mail} placeholder="E-mail" title="Your e-mail address" type="email" className="name" name="mail" /> </label> <br />
                            <label htmlFor="pas" ><input onKeyPress={(e) => { if(e.key === 'Enter') this.login();}} ref={inp => this.pass = inp} placeholder="Password" title="Your password" type="password" className="pas" name="pass" /></label> <br />
                            {this.state.error !== "" && <div className="error"><span>{this.state.error}</span></div>}<br />
                            <button onClick={() => {this.login()}} type="button" className="btn-log-in">Login</button>
                            <button onClick={() => {this.toRegister()}} type="button" className="btn-log-in">To register</button>
                        </form>
                    </div>
                </div>
        );
    }


}
export default connect(
    state => ({ ...state }),
        dispatch => ({ login: (user) => dispatch(loginAction(user)), auth: () => dispatch(authAction()) })
)(withRouter(LoginComponent));