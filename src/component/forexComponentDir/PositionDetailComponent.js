import React from 'react';
import {connect} from "react-redux";
import styled, { keyframes }  from 'styled-components';
import {hideDetail, showFullDetail} from "../../store/action/ForexAction";
import PositionChart from "../positionComponentDir/PositionChart";
import formatDate from "../../util/formatDate";

const show = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;
const hide = keyframes`
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
  }
`;

const PositionDetailPart = styled.div`
    border-radius: 20px;
    width: calc( 100% - 20px );
    height: calc( 100% - 20px );
    border: 1px solid #f006;
    background-color: #0006;
    text-align: center;
    box-shadow: 0px 0px 20px 1px #e3a7a780;
    display: grid;
    padding: 10px;
    grid-template-columns: 50% 50%;
    grid-template-areas:
        "area11 area12"
        "area21 area22"
        "area31 area32"
        "area41 area42"
        "detail detail";
    grid-area: area12;
    position: relative;
    z-index: 1;
    justify-content: center;	
    animation: ${props => props.hide ? hide : show} 0.5s linear;
    opacity: ${props => props.hide ? 0 : 1};
    
    @media ( max-width: 800px){
        grid-area: area31 / area31 / area33 / area33;
        width: auto;
        height: auto;
        margin: 0vh 1vw 1vh 1vw;
    }
`;
const DetailSpan = styled.span`
    grid-area: area${props => props.area};
    color: white;
    align-self: center;
`;

const DetailLink = styled.a`
    grid-area: detail;
    color: white;
    align-self: center;
    text-decoration: none;
`

class PositionDetailComponent extends React.Component{
    constructor(prop){
        super(prop);
    }

    render(){
        if(!this.props.position.detail.full) {
            return (
                <PositionDetail {...this.props}/>
            );
        }
        return(
            <PositionFullDetail {...this.props}/>
        );
    }
}

export default connect(
    state => ({ position: state.position, currency: state.forex.currency }),
    dispatch => ({hideDetail: () => dispatch(hideDetail()), showFullDetail: (number) => dispatch(showFullDetail(number))})
)(PositionDetailComponent);

class PositionDetail extends React.Component{
    render(){
        return(
            <PositionDetailPart hide={this.props.hide} id="positionDetail">
                <DetailSpan area="11" >FROM: </DetailSpan>
                <DetailSpan area="12" >{formatDate(new Date(this.props.position.detail.from))}</DetailSpan>
                <DetailSpan area="21" >TO: </DetailSpan>
                {this.props.position.detail.to !== void 0 && <DetailSpan area="22" >{formatDate(new Date(this.props.position.detail.to))}</DetailSpan> }
                <DetailSpan area="31" >BUY: </DetailSpan>
                <DetailSpan area="32" >{this.props.position.detail.buy.toFixed(this.props.position.detail.buy > 100 ? 0 : 2)} {this.props.currency}</DetailSpan>
                <DetailSpan area="41" >SELL: </DetailSpan>
                {this.props.position.detail.sell !== void 0 &&
                <DetailSpan
                    area="42">{this.props.position.detail.sell.toFixed(this.props.position.detail.sell > 100 ? 0 : 2)} {this.props.currency}</DetailSpan>
                }
                <DetailLink href="javascript::void(0)" onClick={(e) => {e.stopPropagation(); this.props.showFullDetail(1); }}>Show detail</DetailLink>
            </PositionDetailPart>
        );
    }
}

const FullDetailDiv = styled.div`
    position: absolute;
    z-index: 99;
    width: 100%;
    height: 100%;
    background-color: #23353c;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    overflow: hidden;
    display: flex;
    font-family: arial;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;
class PositionFullDetail extends React.Component{
    constructor(prop){
        super(prop);
        this.state = {data: {}};
        console.log(window.innerHeight, window.innerWidth);
    }
    componentDidMount() {
        document.getElementById("loading").className = "";

    }

    render(){
        return(
            <FullDetailDiv >
                <PositionChart detail={this.props.position.detail} currency={"CZK"} data={this.props.position.detail.data} width={window.innerWidth} height={window.innerHeight} />
            </FullDetailDiv>
        );
    }
}