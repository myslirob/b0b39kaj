import React from 'react';
import {connect} from "react-redux";
import styled from "styled-components";
import upImage from "../../images/up.png";
import downImage from "../../images/down.png";
import MarginNavComponent from "./MarginNavComponent";
import {logoutAction} from "../../store/action/UserAction";

const Nav = styled.nav`
    border: 1px solid #f006;
    width: 100%;
    height: 5vh;
    
    background-color: #252121;
    text-align: center;
    box-shadow: 0px 0px 20px 1px #e3a7a780;
    position: absolute;
    display: grid;
    grid-template-columns: 33vw 34vw 33vw;
    grid-template-areas:
        "area1 area2 area3";
    align-items: center;
    justify-content: center;
`;

const Logout = styled.a`
    font-size: 2.6vh;
    text-decoration: none;
    color: white;
`;

class NavComponent extends React.Component{
    constructor(prop){
        super(prop);
        console.clear();
        this.theme = {color: "white", image: "up"};
    }

    logout(e){
        window.location="/"; // for debug
        return;
        this.props.logout();
    }

    componentDidMount() {

    }
    componentWillUpdate(nextProps, nextState, nextContext) {
        this.theme = this.props.forex.cash <= nextProps.forex.cash ? {color: "darkgreen", image: upImage} : {color: "#A5281B", image: downImage};
    }

    render(){
        return(
            <Nav>
                <MarginNavComponent {...this.props} />
                <div className="menu-icon">
                    {/*<img alt="menu" src={require("../../images/menu-icon.png")} width="5vh" height="5vw" ></img>*/}
                    <Logout href="javascript:void(0)" onClick={(e) => this.logout(e)} >LOGOUT</Logout>
                </div>
            </Nav>
        );
    }

}
export default connect(state => ({...state}), dispatch => ({ logout: () => dispatch(logoutAction()) }))(NavComponent);
