import React from 'react';
import {connect} from "react-redux";
import styled from 'styled-components';
import upImage from '../../images/up.png';
import downImage from '../../images/down.png';

const StatusSpan = styled.span`
    font-size: 4vw;
    font-style: italic;
    align-self: center;
    color: whitesmoke;
    margin-bottom: 0;
    text-shadow: 20px 20px 13px black;
    white-space: nowrap;
    transition: all 0.5s ease-in-out 0s, visibility 0s linear 0.5s, z-index 0s linear 0.01s;
    margin-right: 5vw;
    color: ${props => props.theme.color};
    &::after{
        background-image: url(${props => props.theme.image});
        background-size: 4vw 4vw;
        display: inline-block;
        width: 4vw;
        height: 4vw;
        content: "";
        position: absolute;
        margin-left: 0.5vw;
        margin-top: 0.4vw;
        opacity: 0.7;
    }
`;


const MarginDiv = styled.div`
    border-radius: 5px;
    border: 0px solid red;
    background-color: #65434957;
    height: 100px;
    margin: 2vh 2vw 2vh 2vw;
    padding: 1vh 1vw 1vh 1vw;
    max-width: 60vh;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    box-shadow: 0px 0px 20px 1px #ff00003b;
    grid-area: area12;
    align-self: center;
    display: flex;
    align-items: center;
    justify-content: center;
    @media ( max-width: 800px){
        grid-area: area11 / area11 / area13 / area13;
    }
`;

class MarginComponent extends React.Component{
    constructor(prop){
        super(prop);
        //this.state.statusClass === "";
        this.state = {statusClass: ""};
        this.theme = {color: "white", image: "up"};
    }

    componentDidMount() {


    }


    componentWillUpdate(nextProps, nextState, nextContext) {
        this.theme = this.props.forex.cash <= nextProps.forex.cash ? {color: "darkgreen", image: upImage} : {color: "#A5281B", image: downImage};
    }

    render(){
        return(
                <MarginDiv>
                        <StatusSpan theme={this.theme}>
                            {parseFloat(this.props.forex.cash).toFixed(this.props.forex.cash >= 100000 ? 0 :1).replace(/\B(?=(\d{3})+(?!\d))/g, " ")} {this.props.forex.currency}

                        </StatusSpan>
                </MarginDiv>
        );
    }

}
export default connect(
    state => ({ forex: state.forex }))(MarginComponent);