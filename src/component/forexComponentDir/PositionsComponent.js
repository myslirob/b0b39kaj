import React from 'react';
import {connect} from "react-redux";
import styled from 'styled-components';
import {showDetail} from "../../store/action/ForexAction";


const PositionRoot = styled.div`
    padding: 10px;
    margin: 2vh 2vw 2vh 2vw;
    border-radius: 20px;
    border: 1px solid #f006;
    background-color: #252121;
    text-align: center;
    box-shadow: 0px 0px 20px 1px #e3a7a780;
    display: flex;
    flex-direction: column;
    z-index: 5;
    grid-area: area11 / area11 / area21 / area21;
    
    @media ( max-width: 800px){
        grid-area: area11 / area11 / area23 / area23;
    }
`;

class PositionsComponent extends React.Component{


    render(){
        let key = 0;
        return(
            <PositionRoot id="positions">
                <div className="positions-inside">
                    {this.props.position.openPosition !== void 0 && this.props.position.openPosition.map((position) => {
                        return <OpenPositionRow key={key++} {...position} showDetail={this.props.showDetail} position={this.props.position}/>;
                    })}
                    {this.props.position.closedPosition !== void 0 && this.props.position.closedPosition.map((position, index) => {
                        return <ClosedPositionRow key={key++} {...position} showDetail={this.props.showDetail} position={this.props.position}/>;
                    })}
                </div>
            </PositionRoot>
        );
    }

}

const StatePos = styled.span`
    font-size: 1.8vw;
    height: auto;
    min-width: 80px;
    width: 80%;
    border-radius: 5px;
    color: ${props => props.theme };
    background-color: #25232303;
    border: solid 0px red;
    box-shadow: 1px 1px 8px 1px #25232303;
    
`;

class OpenPositionRow extends React.Component{
    constructor(prop){
        super(prop);
        this.state = {active: this.props.order === this.props.position.detail.order ? "active" : ""};
    }

    componentDidMount() {

    }

    showDetail(e){
        //e.stopPropagation();
        this.setState({active: "active"});
        this.props.showDetail(this.props.order, false);
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        this.state.active = this.props.order === nextProps.position.detail.order ? "active" : "";

        return true;
    }

    render(){
        return(
            <div className={`position-row ${this.state.active}`} onClick={(e) => this.showDetail(e)}>
                <div className="position-column">{this.props.brokercode}</div>
                <div className="position-column">{parseFloat(this.props.profit).toFixed(this.props.profit >= 1000 ? 0 :1).replace(/\B(?=(\d{3})+(?!\d))/g, " ")}</div>
                <div className="position-column"><StatePos theme="white">OPEN</StatePos></div>
            </div>
        );
    }

}

class ClosedPositionRow extends React.Component{
    constructor(prop){
        super(prop);
        this.state = {active: this.props.order === this.props.position.detail.order ? "active" : ""};
    }

    componentDidMount() {

    }

    showDetail(e){
        e.stopPropagation();
        this.setState({active: "active"});
        this.props.showDetail(this.props.order, true);
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    render(){
        const active = this.props.order === this.props.position.detail.order ? "active" : "";
        return(
            <div className={`position-row ${active}`} onClick={(e) => this.showDetail(e)}>
                <div className="position-column">{this.props.brokercode}</div>
                <div className="position-column">{parseFloat(this.props.profit).toFixed(this.props.profit >= 1000 ? 0 :1).replace(/\B(?=(\d{3})+(?!\d))/g, " ")}</div>
                <div className="position-column"><StatePos theme={this.props.profit > 0 ? "green" : "red"}>CLOSED</StatePos></div>
            </div>
        );
    }

}

export default connect(
    state => ({ position: state.position }),
    dispatch => ({ showDetail: (number, closed) => dispatch(showDetail(number, closed))})
)(PositionsComponent);