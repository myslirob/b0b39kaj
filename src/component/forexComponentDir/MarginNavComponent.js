import React from 'react';
import {connect} from "react-redux";
import styled, {keyframes} from 'styled-components';
import upImage from '../../images/up.png';
import downImage from '../../images/down.png';

const StatusSpan = styled.span`
    font-size: 4vh;
    font-style: italic;
    align-self: center;
    color: whitesmoke;
    cursor: default;
    margin-bottom: 0;
    white-space: nowrap;
    transition: all 0.5s ease-in-out 0s, visibility 0s linear 0.5s, z-index 0s linear 0.01s;
    margin-right: 5vh;
    color: ${props => props.theme.color};
    &::after{
        background-image: url(${props => props.theme.image});
        background-size: 3.7vh 3.7vh;
        display: inline-block;
        width: 3.7vh;
        height: 3.7vh;
        content: "";
        position: absolute;
        margin-left: 0.5vw;
        margin-top: 0.4vh;
        opacity: 0.7;
    }
`;

const MarginDiv = styled.div`
    align-self: center;
    justify-items: center;
    grid-area: area2;
    margin-bottom: 10px;
    place-self: center;
`;

const show = keyframes`
  from {
    opacity: 0;
    top: -30px;
  }

  to {
    opacity: 1;
    top: 5vh;
  }
`;

const hide = keyframes`
  from {
    opacity: 1;
    top: 5vh;
  }

  to {
    top: -30px;
    opacity: 0;
  }
`;

const MarginDetailDiv = styled.div`
    border-radius: 5px;
    border: 0px solid red;
    background-color: #65434957;
    height: 100px;
    padding: 1vh 1vw 1vh 1vw;
    width: 26vw;
    min-width: 200px;
    left: 37vw;
    text-align: center;
    box-shadow: 0px 0px 20px 1px #ff00003b;
    align-self: center;
    display: grid;
    grid-template-columns: 50% 50%;
    grid-template-areas:
        "area11 area12"
        "area21 area22"
        "detail detail";
    align-items: center;
    justify-content: center;
    position: absolute;
    @media (max-width: 1050px){
        left: 400px;
    }
    @media ( max-width: 800px){
        grid-template-areas:
        "area11 area12 area21 area22";
        grid-template-columns: 25% 25% 25% 25%;
        width: 90vw;
        height: 8vh;
        left: 4vw;
    }
    z-index: -1;
    top: 5vh;
    animation: ${props => !props.hide ? hide : show} 0.5s linear;
    opacity: ${props => !props.hide ? 0 : 1};
`;

const DetailSpan = styled.span`
    grid-area: area${props => props.area};
    color: white;
    align-self: center;
`;

class MarginNavComponent extends React.Component{
    constructor(prop){
        super(prop);
        //this.state.statusClass === "";
        this.state = {statusClass: ""};
        this.theme = {color: "white", image: "up"};
        console.log("MarginDivDetail - props", this.props, prop);
    }

    componentDidMount() {


    }


    componentWillUpdate(nextProps, nextState, nextContext) {
        this.theme = this.props.forex.cash <= nextProps.forex.cash ? {color: "darkgreen", image: upImage} : {color: "#A5281B", image: downImage};
    }

    render(){
        return(
            <MarginDiv >
                <StatusSpan theme={this.theme} onClick={() => {this.props.setDetail(true)}}>
                    {parseFloat(this.props.forex.cash).toFixed(this.props.forex.cash >= 100000 ? 0 :1).replace(/\B(?=(\d{3})+(?!\d))/g, " ")} {this.props.forex.currency}
                </StatusSpan>
                <MarginDetailDiv hide={this.props.detail}>
                    <DetailSpan area="11">
                        Margin:
                    </DetailSpan>
                    <DetailSpan area="12">
                        {parseFloat(this.props.forex.margin).toFixed(this.props.forex.margin >= 100000 ? 0 :1).replace(/\B(?=(\d{3})+(?!\d))/g, " ")} {this.props.forex.currency}
                    </DetailSpan>
                    <DetailSpan area="21">
                        Margin free:
                    </DetailSpan>
                    <DetailSpan area="22">
                        {parseFloat(this.props.forex.marginFree).toFixed(this.props.forex.marginFree >= 100000 ? 0 :1).replace(/\B(?=(\d{3})+(?!\d))/g, " ")} {this.props.forex.currency}
                    </DetailSpan>
                </MarginDetailDiv>
            </MarginDiv>
        );
    }

}
export default connect(
    state => ({ forex: state.forex }))(MarginNavComponent);