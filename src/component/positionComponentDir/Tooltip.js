import { Tooltip } from '@vx/tooltip';
import React, { Component }  from 'react';

export default ({ yTop, yLeft, yLabel, xTop, xLeft, xLabel }) => {
    return (
        <div>
            <Tooltip
                top={xTop}
                left={xLeft}
                style={{
                    transform: 'translateX(-50%)',
                    "zIndex": 1000,
                    position: "absolute",
                }}
            >
                {xLabel}
            </Tooltip>
            <Tooltip
                top={yTop}
                left={yLeft}
                style={{
                    backgroundColor: 'rgba(92, 119, 235, 1.000)',
                    color: 'white',
                    "zIndex": 1000,
                    position: "absolute",
                }}
            >
                {yLabel}
            </Tooltip>
        </div>
    );
};