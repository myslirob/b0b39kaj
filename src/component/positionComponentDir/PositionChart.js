import Chart from './ChartComponent';
import formatPrice from '../../util/formatPrice';
import React, { Component }  from 'react';
import styled from 'styled-components';
import formatDate from "../../util/formatDate";


const ChartDiv = styled.div`
          color: white;
          background-color: #27273f;
          border-radius: 6px;
          box-shadow: 0 2px 10px rgba(0, 0, 0, 0.7);
          display: flex;
          flex-direction: column;
          width: 70vw;

`;
export default class PositionChart extends React.Component {


        constructor(prop)
        {
            super(prop);
            this.prices = Object.keys(this.props.data).map(k => ({
                time: k,
                price: this.props.data[k],
            }));
            this.currentPrice = this.prices[this.prices.length - 1].price;
            this.firstPrice = this.prices[0].price;
            this.diffPrice = this.currentPrice - this.firstPrice;
            this.hasIncreased = this.diffPrice > 0;
        }
    render() {
        return (
            <ChartDiv onClick={(e) => e.stopPropagation()}>
                <div className="title">
                    <div>
                        {this.props.currency}<br/>
                        <small>{formatDate(new Date(this.props.detail.from))} - {this.props.detail.to !== void 0 && formatDate(new Date(this.props.detail.to))}</small>
                    </div>
                    <div className="spacer">Order: {this.props.detail.order}</div>
                    <div className="stats">
                        <div className="current">
                            {formatPrice(this.currentPrice)} {this.props.currency}
                        </div>
                        <div className={this.hasIncreased ? 'diffIncrease' : 'diffDecrease'}>
                            {this.hasIncreased ? '+' : '-'}
                            {formatPrice(this.diffPrice)} {this.props.currency}
                        </div>
                    </div>
                </div>
                <div className="chart">
                    <Chart
                        currency={this.props.currency}
                        data={this.prices}
                        parentWidth={this.props.width * 0.6}
                        parentHeight={this.props.height * 0.45}
                        margin={{
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 45
                        }}
                    />
                </div>
                <style>{`
        .duration {
          font-weight: 100 !important;
          font-size: 14px;
          padding-bottom: 1px;
          border-bottom: 2px solid #6086d6;
        }
        .title,
        .stats {
          padding: 15px 15px 0;
          display: flex;
          flex-direction: row;
          align-items: center;
        }
        .title small {
          color: #6086d6;
        }
        .stats {
          padding: 0px;
          justify-content: flex-end;
          align-items: flex-end;
          flex-direction: column;
        }
        .current {
          font-size: 24px;
        }
        .diffIncrease,
        .diffDecrease {
          font-size: 12px;
          margin-left: .5rem;
        }
        .diffIncrease {
          color: #00f1a1;
        }
        .spacer {
          display: flex;
          flex: 1;
          flex-direction: row;
          justify-content: center;
        }
        .chart {
          display: flex;
          flex: 1;
          width: auto;
          margin: 0 auto;
        }
      `}</style>
            </ChartDiv>
        );
    }
}