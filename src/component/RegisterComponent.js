import React from 'react';
import {withRouter} from "react-router";
import {connect} from "react-redux";
import Routing from "../util/Routing";
import Routes from "../util/Routes";
import {registerAction} from "../store/action/UserAction";

class RegisterComponent extends React.Component{
    constructor(prop){
        super(prop);
        this.state = {error: ""};
        this.inputs = {};
        this.verified = false;
    }
    register(){
        //this.tryAccount();
        let user = {};
        if(this.verified === false){
            this.tryAccount();
        }
        for(let input in this.inputs) {
            if (!this.inputs[input].checkValidity()) {
                this.inputs[input].parentElement.className = "failed";
                this.verified = false;
            }
            user[input] = this.inputs[input].value;
        }
        if (this.inputs.pass.value.length < 5) {
            this.inputs.pass.parentElement.className = "failed";
            this.verified = false;
        }
        if(this.verified){
            this.props.register(user)
                .then((response) => {
                    if(response.status === 0)
                        this.toLogin();
                    else
                        this.setState({error: response.message});
                }).catch((res) => {
                    this.setState({error: "Server error"});
                });
        }
    }
    toLogin(){
        const center = document.getElementsByClassName("center").item(0);
        center.classList.add("rotate-out");
        setTimeout(() => { Routing.transitionTo(Routes.login); }, 900);
    }

    componentDidMount() {
        const center = document.getElementsByClassName("center").item(0);
        center.classList.add("rotate-in");
        setTimeout(()=> {
            center.classList.remove("rotate-in");
        }, 900);
    }

    tryAccount(){
        if(this.inputs.xtbpass.checkValidity() && this.inputs.number.checkValidity()) {
            let websocket = new WebSocket("wss://ws.xapi.pro/demo");
            let credentials = {
                "command": "login",
                "arguments": {
                    "userId": this.inputs.number.value,
                    "password": this.inputs.xtbpass.value,
                }
            };
            websocket.onmessage = (e) => {
                if (JSON.parse(e.data).status === true) {
                    this.verified = true;
                    this.inputs.xtbpass.parentElement.className = "success";
                    this.inputs.number.parentElement.className = "success";
                } else {
                    this.verified = false;
                    this.inputs.xtbpass.parentElement.className = "failed";
                    this.inputs.number.parentElement.className = "failed";
                }
                websocket.close();
            };
            websocket.onopen = () => {
                websocket.send(JSON.stringify(credentials));
            };
            return true;
        }
        return false;
    }

    render(){
        return(
                <div className="center">
                    <h1>Register</h1>
                    <div className="form" >
                        <form method="post" action="" >
                            <label htmlFor="mail" className="label-name"><input ref={inp => this.inputs.mail = inp}
                                onBlur={(e) => {if(e.target.checkValidity()) e.target.parentElement.className = "success"; else  e.target.parentElement.className = "failed";}}
                                placeholder="E-mail" title="Your e-mail address" type="email" className="name" name="mail" id="mail" required={true}/>
                            </label> <br/>
                            <label htmlFor="pas"><input ref={inp => this.inputs.pass = inp} placeholder="System password" minLength="5" required={true}
                                onBlur={(e) => {if(e.target.checkValidity()) e.target.parentElement.className = "success"; else  e.target.parentElement.className = "failed";}}
                                title="Your password" type="password" className="pas" name="pass" id="pas"/>
                            </label> <br/>
                            <label htmlFor="number" className="label-name">
                                <input minLength="8" ref={inp => this.inputs.number = inp} onChange={(e) => this.verified = false}  placeholder="Broker number" onBlur={(e) => { this.tryAccount()}}
                                       title="Your broker account number" type="text" className="name" name="number" id="number" required={true} />
                            </label> <br />
                            <label htmlFor="xtbpas" >
                                <input minLength="3" ref={inp => this.inputs.xtbpass = inp} onChange={(e) => this.verified = false} placeholder="Broker password" onBlur={(e) => { this.tryAccount()}}
                                       title="Your broker account password" type="password" className="pas" name="xtbpassword" id="xtbpas" required={true} />
                            </label> <br />
                            {this.state.error !== "" && <div className="error"><span>{this.state.error}</span></div>}<br />
                            <button onClick={() => {this.register()}} type="button" className="btn-log-in">Register</button>
                            <button onClick={() => {this.toLogin()}} type="button" className="btn-log-in">Back to login</button>
                        </form>
                    </div>
                </div>
        );
    }


}
export default connect(state => ({ ...state }), dispatch => ({ register: (user) => dispatch(registerAction(user)) }))(withRouter(RegisterComponent));