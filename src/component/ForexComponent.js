import React from 'react';
import {withRouter} from "react-router";
import {connect} from "react-redux";
import MarginComponent from "./forexComponentDir/MarginComponent";
import PositionsComponent from "./forexComponentDir/PositionsComponent";
import NavComponent from "./forexComponentDir/NavComponent";
import {authAction} from "../store/action/UserAction";
import {refreshIntervalAction, debugInterval, hideDetail, showFullDetail} from "../store/action/ForexAction";
import styled from "styled-components";
import Routing from "../util/Routing";
import Routes from "../util/Routes";
import PositionDetailComponent from "./forexComponentDir/PositionDetailComponent"


const ForexContainer = styled.div`
    
            display: grid;
            justify-content: stretch;	
            grid-template-columns: minmax(390px, 33vw) 1fr 1fr;
            grid-template-rows: 10vh 30vh 30vh 30vh;
            grid-template-areas: 
                "header header header"
                "area11 area12 area13"
                "area21 area22 area23"
                "area31 area32 area33";
            @media ( max-width: 800px){
                grid-column-gap: 0;
                grid-row-gap: 0;
                grid-template-columns: 32% 33% auto;
                grid-template-rows: 13vh 32vh 33vh 22vh;
            
            }
`

class ForexComponent extends React.Component{
    constructor(prop){
        super(prop);
        this.state = { hideDetail: false, detail: false};

    }

    componentDidMount() {
        this.audio.play();
        setTimeout( () => {
            document.getElementById("loading").className = "";
        }, 1000);
        this.props.debugInterval();
        return;// For debug without server
        try {
            this.props.refresh(this.props.user.number);
        } catch(e){
            clearInterval(e);
            Routing.transitionTo(Routes.login);
        }
    }

    setDetail = (detail) => {
        console.log(this, this.setState);
        this.setState({detail: detail});
    }

    hideAll(e){
        if(this.props.detail!== false)
            this.hideDetail(e);
        else if(this.state.detail !== false)
            this.setState({detail: false});
    }

    hideDetail = (e) => {
        if(this.props.detail!== false){
            this.setState({hideDetail: true});
            setTimeout(() => {
                this.props.hideDetail();
                this.setState({hideDetail: false});
            }, 500);
        }
    }

    render(){
        return(
            <ForexContainer onClick={(e) => this.hideAll(e)}>
                <audio ref={(e) => { this.audio = e}} >
                    <source src="audio.mp3" type="audio/mpeg" />
                </audio>
                <NavComponent detail={this.state.detail} setDetail={this.setDetail}/>
                {this.props.detail !== false && <PositionDetailComponent hide={this.state.hideDetail}/>}
                <PositionsComponent/>
            </ForexContainer>
        );
    }

}
export default connect(
    state => ({ user: state.user, detail: state.position.detail }),
    dispatch => ({hideDetail: () => dispatch(hideDetail()), refresh: (number) => dispatch(refreshIntervalAction(number)),
        auth: () => dispatch(authAction()), debugInterval: () => dispatch(debugInterval()), showFullDetail: (number) => dispatch(showFullDetail(number)) })
)(withRouter(ForexComponent));