import Routing from "../../util/Routing";
import Routes from "../../util/Routes";
import store from "../Store";

const axios = require('axios');
axios.defaults.withCredentials = true;
export const refreshIntervalAction = (number) => dispatch => {
    console.log(this);

    const intervalID =  setInterval(() => {
        console.log("number: ", number);
        axios.post("http://localhost:8090/report", {number: number}).then((res) => {
            console.log(res, res.data.data);
            if(Object.keys(res.data.data).length === 0){
                clearInterval(intervalID);
                Routing.transitionTo(Routes.login);
            }
            dispatch({
                type: "openPosition",
                payload: res.data.data.openPosition,
            });
            dispatch({
                type: "setMargin",
                payload: {
                    currency: res.data.data.currency,
                    cash: res.data.data.cash,
                }
            });
        }).catch(res => {
            console.log(res);
            clearInterval(intervalID);
            Routing.transitionTo(Routes.login);
            //throw 0;
        });
    }, 1000);

};

export const showDetail = (number, closed) => dispatch => {
    let detail = closed ? store.getState().position.closedPosition : store.getState().position.openPosition;
    detail = detail.filter((p) => p.order === number)[0];
    console.log(detail, number);
    if(detail !== void 0) {
        dispatch({
            type: "setDetail",
            payload: {
                full: false,
                order: number,
                from: detail.from,
                to: detail.to,
                buy: detail.buy,
                sell: detail.sell,
                brokercode: detail.brokercode,
                data: detail.data,
            },
        });
    }
}

export const showFullDetail = (number) => dispatch => {
    document.getElementById("loading").className = "active";
    const axiosPromise = new Promise((resolve, reject) => {
        setTimeout(() => resolve(), 1000);
    }).then(res => {
    //     fetch('https://api.coindesk.com/v1/bpi/historical/close.json?start=2013-09-01&end=2018-09-05')
    //         .then(res => {
    //             return res.json();
    //         })
    //         .then(json => {
    //             dispatch({
    //                 type: "setDetail",
    //                 payload: {
    //                     data: json,
    //                     full: true,
    //                 },
    //             });
    //         });
        dispatch({
            type: "setDetail",
            payload: {
                full: true,
            },
        });
    });
}

export const hideDetail = () => dispatch => {
    dispatch({
        type: "setDetail",
        payload: false,
    });
}


export const debugInterval = () => dispatch => {
    setInterval(() => {
        let temp = Math.random()*100000*2;
        let marginFree = Math.random()*100000;
        let positions = store.getState().position.openPosition;
        let closedPositions = [];
        for(let x = 0; x < positions.length; x++){
            if(Math.random() > 0.93){
                positions[x].to = new Date().toLocaleString();
                positions[x].sell = positions[x].buy+positions[x].profit;
                closedPositions.unshift(positions[x]);
                positions.splice(x, 1);
            } else {
                positions[x].profit = positions[x].profit + Math.random()*95 - Math.random()*48;
                positions[x].data[new Date().toLocaleString()] =  positions[x].profit+positions[x].buy;
            }
        }
        if(Math.random() > 0.8){
            const price = Math.random()*952;
            positions.unshift({brokercode: "EURUSD", buy: price,  profit: 0, order: Math.random()*856972, from: new Date().toLocaleString(),
                data: {[new Date().toLocaleString()]: price}
            });
        }
        dispatch({
                type: "position",
                payload: {open: positions, close: closedPositions},
            }
        );
        dispatch({
                    type: "setMargin",
                    payload: {
                        currency: "CZK",
                        cash: temp,
                        marginFree: marginFree,
                        margin: temp-marginFree,
                    }
                });
    }, 1000);
}
