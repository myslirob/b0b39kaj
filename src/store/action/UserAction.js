const axios = require('axios');
axios.defaults.withCredentials = true;
export const registerAction = (user) => dispatch => {
    return axios.post("http://localhost:8090/register", user)
        .then((response) => {
            if(response.data.status === 0){
                dispatch({
                    type: 'setUser',
                    payload: {mail: user.mail},
                });

            }
            return response.data;
        });

};

export const authAction = () => dispatch => {
    return axios.get("http://localhost:8090/userauth").then((res) => {
        console.log("AUTH: ", res);
        if(res.data.status === "AUTHORIZED") {
            dispatch({
                type: 'setUser',
                payload: {mail: res.data.username, number: res.data.number},
            })
        }
        return res;
    });
}

export const loginAction = (user) => dispatch => {
    const params = new URLSearchParams();
    params.append("mail", user.mail);
    params.append("pass", user.pass);
    return axios.post("http://localhost:8090/login", params)
        .then((response) => {
            console.log("Login response", response);
            if(response.data.success) {
                dispatch({
                    type: 'setUser',
                    payload: {mail: response.data.username, number: response.data.number},
                });
            }
            return response.data;

        });
};

export const logoutAction = () => dispatch => {
    return axios.post("http://localhost:8090/logout")
        .then((response) => {
            window.location="/";
        });
};