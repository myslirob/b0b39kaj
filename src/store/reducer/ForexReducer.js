
const initialState = {
    currency: "CZK",
    cash: Number(9999.13),
};

export const forex = (state = initialState, action) => {
    switch (action.type) {
        case 'setMargin':
            return action.payload;
        default:
            return state
    }
}