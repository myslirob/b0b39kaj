import { combineReducers } from 'redux';
import {user} from './UserReducer';
import {forex} from './ForexReducer';
import {position} from "./PositionReducer";

export default combineReducers({
    user, forex, position,
});