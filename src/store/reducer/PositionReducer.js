
const initialState = {
    detail: false,
    openPosition: [],
    closedPosition: [],
};

export const position = (state = initialState, action) => {
    switch (action.type) {
        case 'setDetail':
            return Object.assign({}, state, {detail: action.payload !== false ? Object.assign({}, state.detail, action.payload) : false});
        case 'setDetailData':
            return Object.assign({}, state, {detail: Object.assign({}, state.detail, action.payload)});
        case 'position':
            return Object.assign({}, state, {openPosition: action.payload.open, closedPosition: action.payload.close.concat(state.closedPosition)});
        default:
            return state
    }
}