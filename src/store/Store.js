import {applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import MainReducer from './reducer/MainReducer';

const loggerMiddleware = createLogger();

const initialState = {user: {mail: ""}};

const store = createStore(MainReducer, initialState, applyMiddleware(thunk, loggerMiddleware));

export default store;