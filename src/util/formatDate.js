import { timeFormat } from 'd3-time-format';
export default timeFormat('%e.%m %H:%M:%S');