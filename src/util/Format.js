import { format } from 'd3-format';
import { timeFormat } from 'd3-time-format';
export const formatPrice = (fp, currency) => { return format('$,.2f'); };
export const formatDate = () => { return timeFormat('%b %d'); };